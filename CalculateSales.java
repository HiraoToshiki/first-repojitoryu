package jp.alhinc.hirao_toshiki.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		try {
			//支店定義ファイルの読み込み準備
			File file = new File("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\branch.lst");
			BufferedReader br =new BufferedReader(new FileReader(file));
			String brancLine = null;
			String[] branchCode = null;
			String[] branchBranch = null;
			List<String> branchCodeList = new ArrayList<String>();
			List<String> branchBranchList = new ArrayList<String>();
			//支店定義ファイルの読みこみ
			while ((brancLine = br.readLine())  != null) {
				String[] array = brancLine.split(",");
				if(array.length == 2) {
					branchCodeList.add(array[0]);
					branchBranchList.add(array[1]);
			}
			//読み込み結果確認
				branchCode = (String[])branchCodeList.toArray(new String[0]);
				branchBranch = (String[])branchBranchList.toArray(new String[1]);
				System.out.println("支店定義ファイル_コード");
				for (String theBranchCode : branchCode) {
				System.out.println(theBranchCode);
				}
				System.out.println();
				System.out.println("支店定義ファイル_支社名");
				
				for (String theBranchBranch : branchBranch) {
					System.out.println(theBranchBranch);
				} 
				}
			//売上ファイルの
			//売上ファイルの読み込み準備
			File file2 = new File("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\00000001.rcd");
			BufferedReader br2 =new BufferedReader(new FileReader(file2));
			String salesLine = null;
			String[] salesCode = null;
			String[] salesMoney = null;
			List<String> salesCodeList = new ArrayList<String>();
			List<String> salesMoneyList = new ArrayList<String>();
			//売上ファイルの読みこみ
			while ((salesLine = br2.readLine())  != null) {
				String[] array = salesLine.split("CRLF");
				if(array.length == 2) {
					salesCodeList.add(array[0]);
					salesMoneyList.add(array[1]);
			}
			//読み込み結果確認
				salesCode = (String[])salesCodeList.toArray(new String[0]);
				salesMoney = (String[])salesMoneyList.toArray(new String[1]);
				System.out.println("売上ファイル_コード");
				for (String theSalesCode : salesCode) {
				System.out.println(theSalesCode);
				}
				System.out.println();
				System.out.println("売上ファイル_売上額");
				
				for (String theSalesMoney : salesMoney) {
					System.out.println(theSalesMoney);
				} 
				}
		}
			catch (FileNotFoundException e) {		
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
				//支店定義ファイルを閉じる
				
				}
		}
	{
}
}

/*			
		
		File branchFile = new File("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\branch.lst");
		BufferedReader branch =null;
		
		
		//支店定義ファイル有無確認
		if (!branchFile.exists()) {
			System.out.println("支店定義ファイルは存在しません。");
			return;
		}
		//売上ファイルのパス定義（正規表現に修正必要）
		File salesFile = new File("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\00000001.rcd");
		//売上ファイルのパス定義
		if (!salesFile.exists()) {
			System.out.println("売上ファイルは存在しません。");
			return;
		}
		BufferedReader branch =null;
		//支店定義ファイルの支店コード、支店名を入れる配列
		//売上ファイルの支店コード、売上額を入れる配列
		String[] branchCode = null;
		String[] branchBranch = null;
		String[] SalesCode = null;
		String[] SalesSales = null;
		List<String> branchCodeList = new ArrayList<String>();
		List<String> branchBranchList = new ArrayList<String>();
		List<String> SalesCodeList = new ArrayList<String>();
		List<String> SalesSalesList = new ArrayList<String>();
		//支店定義ファイル、売上ファイルの読み込み処理
		read(branchCodeList, branchBranchList, SalesCodeList, SalesSalesList);
		//支店定義ファイルで読み込んだ情報を配列に入れる
		branchCode = (String[])branchCodeList.toArray(new String[0]);
		branchBranch = (String[])branchBranchList.toArray(new String[0]);
		//売上ファイルで読み込んだ情報を配列に入れる
		SalesCode = (String[])SalesCodeList.toArray(new String[0]);
		SalesSales = (String[])SalesSalesList.toArray(new String[0]);
		//支店定義ファイルの出力（配列に期待通りの文字列が入っているか確認）
		System.out.println("コード");
		for (String theBranchCode : branchCode) {
		System.out.println(theBranchCode);
		}
		System.out.println();
		System.out.println("支社名");
		for (String theBranchBranch : branchBranch) {
			System.out.println(theBranchBranch);
		}
		//売上ファイルの出力（配列に期待通りの文字列が入っているか確認）
		System.out.println("コード");
		for (String theSalesCode : SalesCode) {
		System.out.println(theSalesCode);
		}
		System.out.println();
		System.out.println("売上額");
		for (String theSalesSales : SalesSales) {
			System.out.println(theSalesSales);
		}		
		
				
		
		
		
		}
	//ファイル読込処理
	private static void read(List<String> branchCodeList, List<String> branchBranchList, List<String> SalesCodeList, List<String> SalesSalesList) {
		//支店定義ファイル
		BufferedReader branch =null;
		BufferedReader Sales =null;
		try {
			branch = new BufferedReader(new FileReader("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\branch.lst"));
			Sales = new BufferedReader(new FileReader("C:\\Users\\hope2\\Documents\\01_ALH\\研修\\ProgramBasisTraning\\00000001.rcd"));
		String brancLline = null;
		String salesLine =  null;
		//支店定義ファイルの読みこみ
		while ((brancLline = branch.readLine()) != null) {
			String[] array = brancLline.split(",");
			if(array.length == 2) {
				branchCodeList.add(array[0]);
				branchBranchList.add(array[1]);
		}
		}
		//売上ファイルの読み込み
		while ((salesLine = Sales.readLine()) != null) {
			String[] array = salesLine.split(",");
			if(array.length == 1) {
				SalesCodeList.add(array[0]);

		}
		}
		// エラー処理
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		//支店定義ファイルを閉じる
		try {
		if(branch != null) {
			branch.close();
		}
		}catch (IOException e) {
		e.printStackTrace();
		}
		}
		}
	}*/